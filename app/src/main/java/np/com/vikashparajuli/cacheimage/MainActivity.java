package np.com.vikashparajuli.cacheimage;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import np.com.vikashparajuli.cacheallimage.CacheAllImage;

public class MainActivity extends AppCompatActivity {

    String url = "https://avatars0.githubusercontent.com/u/6658397?v=3&u=ee09771212ef1838fb9f994630f97728a69c6211&s=140";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnLoad = (Button) findViewById(R.id.btnLoad);
        final ImageView ivImage = (ImageView) findViewById(R.id.ivImage);

        btnLoad.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                CacheAllImage.with(getApplicationContext()).load(url, ivImage);
            }
        });
    }
}
