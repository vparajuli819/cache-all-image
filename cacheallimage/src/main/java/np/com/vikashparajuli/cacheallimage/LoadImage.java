package np.com.vikashparajuli.cacheallimage;

import android.widget.ImageView;

/**
 * Created by viks on 2/22/16.
 */
public interface LoadImage {

    public void load(String url, ImageView imageView);
}
