package np.com.vikashparajuli.cacheallimage;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.widget.ImageView;
import android.widget.Toast;

import java.io.InputStream;
import java.net.URL;

/**
 * Created by viks on 2/22/16.
 */
public class CacheAllImage implements LoadImage {

    static Context mContext;
    String url;
    ImageView imageView;

    public static CacheAllImage with(Context context) {
        mContext = context;
        return new CacheAllImage();
    }

    @Override
    public void load(String url, ImageView imageView) {
        this.imageView = imageView;
        this.url = url;
        new LoadImageInOtherThread().execute(url);
    }

    private class LoadImageInOtherThread extends AsyncTask<String, String, Bitmap> {

        Bitmap bitmap;

        protected Bitmap doInBackground(String... args) {
            try {
                bitmap = BitmapFactory.decodeStream((InputStream) new URL(url).getContent());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return bitmap;
        }

        protected void onPostExecute(Bitmap image) {

            if (image != null) {
                imageView.setImageBitmap(bitmap);

            } else {
                Toast.makeText(mContext, "Image Does Not exist or Network Error", Toast.LENGTH_SHORT).show();

            }
        }
    }
}
